﻿using Dyd.BaseService.ServiceCenter.Domain;
using Dyd.BaseService.ServiceCenter.Domain.Bll;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using Dyd.BaseService.ServiceCenter.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Web.Controllers
{
    [Authorize]
    public class NodeReportController : Controller
    {
        #region List
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search">查询实体</param>
        /// <param name="pno">页码</param>
        /// <param name="pagesize">页大小</param>
        /// <returns></returns>
        public ActionResult NodeReportIndex(tb_node_report_model_search search)////所有参数memorysize,errorcount,connectioncount,visitcount,processthreadcount,processcpuper,
        {
            List<TimeChartModel> ms = new List<TimeChartModel>();
            if (string.IsNullOrWhiteSpace(search.columns))
            {
                search.columns = "processcpuper,memorysize,processthreadcount,visitcount,connectioncount,errorcount,";
            }
            if (string.IsNullOrWhiteSpace(search.columns))
            {
                return View(ms);
            }

            if (string.IsNullOrEmpty(search.queryDay))
            {
                search.queryDay = DateTime.Now.ToString("yyyy-MM-dd");
            }
            if (string.IsNullOrWhiteSpace(search.datatype))
            {
                search.datatype = TimeChartDataType.avg.ToString();
            }

            ViewBag.datatype = search.datatype;
            ViewBag.columns = search.columns;
            ViewBag.nodeid = search.nodeid;
            ViewBag.BeginDate = search.queryDay;
            search.TimeChartType = "Day";


            TimeChartDataType chartdatatype = (TimeChartDataType)Enum.Parse(typeof(TimeChartDataType), search.datatype);
            TimeChartType charttype = TimeChartType.Day;

            var startDate = DateTime.Parse(search.queryDay).Date;

            SPagedList<tb_node_report_model> pagelist;
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterReportConnectString))
            {
                conn.Open();
                int total = 0;
                IList<tb_node_report_model> list = new List<tb_node_report_model>();
                list = tb_node_report_bll.Instance.GetList(conn, search);
                pagelist = new SPagedList<tb_node_report_model>(list, search.Pno, search.PageSize, total);
            }

            string[] cs = search.columns.TrimEnd(',').Split(',');
            string unit = string.Empty;
            foreach (var c in cs)
            {
                string Title = string.Empty;

                Dictionary<DateTime, double> dic = new Dictionary<DateTime, double>();

                if (c == "errorcount")
                {
                    Title = "错误数量";
                    unit = "个";
                    foreach (var item in pagelist)
                    {
                        dic.Add(Convert.ToDateTime(item.createtime), Convert.ToDouble(item.errorcount));
                    }
                }

                if (c == "connectioncount")
                {
                    Title = "当前连接数";
                    unit = "个";
                    foreach (var item in pagelist)
                    {
                        dic.Add(Convert.ToDateTime(item.createtime), Convert.ToDouble(item.connectioncount));
                    }
                }

                if (c == "visitcount")
                {
                    unit = "次";
                    Title = "访问次数";
                    foreach (var item in pagelist)
                    {
                        dic.Add(Convert.ToDateTime(item.createtime), Convert.ToDouble(item.visitcount));
                    }
                }

                if (c == "processthreadcount")
                {
                    unit = "个";
                    Title = "线程数";
                    foreach (var item in pagelist)
                    {
                        dic.Add(Convert.ToDateTime(item.createtime), Convert.ToDouble(item.processthreadcount));
                    }
                }

                if (c == "processcpuper")
                {
                    unit = "百分比";
                    Title = "cpu占用";
                    foreach (var item in pagelist)
                    {
                        dic.Add(Convert.ToDateTime(item.createtime), Convert.ToDouble(item.processcpuper.ToString("N2")));
                    }
                }

                if (c == "memorysize")
                {
                    unit = "M";
                    Title = "内存占用";
                    foreach (var item in pagelist)
                    {
                        dic.Add(Convert.ToDateTime(item.createtime), Convert.ToDouble(item.memorysize.ToString("N2")));
                    }
                }

                if (c == "")
                    continue;
                var model = new TimeChartModel();
                model.DataType = chartdatatype;
                model.Type = charttype;
                model.StartDate = startDate;
                model.Key = c;
                model.Title = Title;
                model.UnitText = unit;

                model.SubTitle = model.Title + "【" + model.DataType.ToString() + "】值统计图";
                if (model.Type == TimeChartType.Day)
                {
                    model.PointInterval = "60 * 1000";
                    model.EndDate = model.StartDate.AddDays(1);
                    model.MaxZoom = "60 * 60 * 1000";
                    model.SubTitle += ",采集单位【分钟】";
                }
                else if (model.Type == TimeChartType.Month)
                {
                    model.PointInterval = "24 * 60 * 60 * 1000";
                    model.EndDate = model.StartDate.AddMonths(1);
                    model.MaxZoom = "10 * 24 * 60 * 60 * 1000";
                    model.SubTitle += ",采集单位【天】";
                }
                else if (model.Type == TimeChartType.Year)
                {
                    model.PointInterval = "24 * 60 * 60 * 1000";
                    model.EndDate = model.StartDate.AddYears(1);
                    model.MaxZoom = "10 * 24 * 60 * 60 * 1000";
                    model.SubTitle += ",采集单位【天】";
                }

                DateTime dtbegin = model.StartDate;
                DateTime dtend = model.EndDate;
                while (true)
                {
                    if (!dic.ContainsKey(dtbegin))
                    {
                        dic.Add(dtbegin, 0);
                    }
                    if (model.Type == TimeChartType.Day)
                        dtbegin = dtbegin.AddMinutes(1);
                    else
                        dtbegin = dtbegin.AddDays(1);
                    if (dtbegin >= dtend)
                        break;
                }
                model.DataJson = new XXF.Serialization.JsonHelper().Serializer(dic.OrderBy(c1 => c1.Key).Select(c1 => c1.Value));
                ms.Add(model);
            }
            return View(ms);
        }
        #endregion
    }
}