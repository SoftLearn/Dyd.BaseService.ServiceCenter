﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    //tb_client
    public class tb_client
    {

        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// serverid
        /// </summary>
        [Display(Name = "serverid")]
        public int serverid { get; set; }
        /// <summary>
        /// 会话id
        /// </summary>
        [Display(Name = "会话id")]
        public long sessionid { get; set; }
        /// <summary>
        /// 项目名
        /// </summary>
        [Display(Name = "项目名")]
        public string projectname { get; set; }
        /// <summary>
        /// ip地址
        /// </summary>
        [Display(Name = "ip地址")]
        public string ip { get; set; }
        /// <summary>
        /// 客户端心跳时间
        /// </summary>
        [Display(Name = "客户端心跳时间")]
        public DateTime clientheartbeattime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }

    }

    //tb_client
    public class tb_client_search : BaseSearch
    {
        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// serverid
        /// </summary>
        [Display(Name = "serverid")]
        public int serverid { get; set; }
        /// <summary>
        /// 会话id
        /// </summary>
        [Display(Name = "会话id")]
        public long sessionid { get; set; }
        /// <summary>
        /// 项目名
        /// </summary>
        [Display(Name = "项目名")]
        public string projectname { get; set; }
        /// <summary>
        /// ip地址
        /// </summary>
        [Display(Name = "ip地址")]
        public string ip { get; set; }
        /// <summary>
        /// 客户端心跳时间
        /// </summary>
        [Display(Name = "客户端心跳时间")]
        public DateTime clientheartbeattime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }

    }
}