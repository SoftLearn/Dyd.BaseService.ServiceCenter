﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ServiceCenter.Domain.Model;

namespace Dyd.BaseService.ServiceCenter.Domain.Dal
{
    //tb_log
    public partial class tb_log_dal
    {
        #region C

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_log model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into tb_log(");
            strSql.Append("serviceid,logtype,msg,createtime");
            strSql.Append(") values (");
            strSql.Append("@serviceid,@logtype,@msg,@createtime");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            par.Add(new ProcedureParameter("@serviceid", model.serviceid));
            par.Add(new ProcedureParameter("@logtype", model.logtype));
            par.Add(new ProcedureParameter("@msg", model.msg));
            par.Add(new ProcedureParameter("@createtime", model.createtime));


            object obj = conn.ExecuteSql(strSql.ToString(), par);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_log model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tb_log set ");

            strSql.Append(" serviceid = @serviceid , ");
            strSql.Append(" logtype = @logtype , ");
            strSql.Append(" msg = @msg , ");
            strSql.Append(" createtime = @createtime  ");
            strSql.Append(" where id=@id ");

            par.Add(new ProcedureParameter("@id", model.id));
            par.Add(new ProcedureParameter("@serviceid", model.serviceid));
            par.Add(new ProcedureParameter("@logtype", model.logtype));
            par.Add(new ProcedureParameter("@msg", model.msg));
            par.Add(new ProcedureParameter("@createtime", model.createtime));

            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tb_log ");
            strSql.Append(" where id=@id");
            par.Add(new ProcedureParameter("@id", id));
            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Clear(DbConn PubConn)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            string Sql = "truncate table tb_log ";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Q
        /// <summary>
        /// 重复规则
        /// </summary>
        public bool IsExists(DbConn conn, tb_log model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_category with (nolock) s where 1=1");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_log Get(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, serviceid, logtype, msg, createtime  ");
            strSql.Append(" from tb_log with (nolock)");
            strSql.Append(" where id=@id");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, strSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="search">查询实体</param>
        /// <param name="totalCount">总条数</param>
        /// <returns></returns>
        public IList<tb_log> GetPageList(DbConn conn, tb_log_search search, out int totalCount)
        {
            totalCount = 0;
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            IList<tb_log> list = new List<tb_log>();
            long startIndex = (search.Pno - 1) * search.PageSize + 1;
            long endIndex = startIndex + search.PageSize - 1;
            par.Add(new ProcedureParameter("startIndex", startIndex));
            par.Add(new ProcedureParameter("endIndex", endIndex));

            StringBuilder baseSql = new StringBuilder();
            string sqlWhere = " where 1=1 ";
            baseSql.Append("select row_number() over(order by a.id desc) as rownum,");
            #region Query
            if (search.logtype > 0)
            {
                sqlWhere += " and logtype=@logtype";
                par.Add(new ProcedureParameter("logtype", search.logtype));
            }

            if (search.serviceid > 0)
            {
                sqlWhere += " and serviceid=@serviceid";
                par.Add(new ProcedureParameter("serviceid", search.serviceid));
            }

            #endregion
            baseSql.Append(" a.*,b.servicename ");
            baseSql.Append(" FROM tb_log as a with(nolock) left join tb_service as b with(nolock) on a.serviceid = b.id");

            string execSql = "select * from(" + baseSql.ToString() + sqlWhere + ")  as s where rownum between @startIndex and @endIndex";
            string countSql = "select count(1) from tb_log with(nolock)" + sqlWhere;
            object obj = conn.ExecuteScalar(countSql, par);
            if (obj != DBNull.Value && obj != null)
            {
                totalCount = LibConvert.ObjToInt(obj);
            }
            DataTable dt = conn.SqlToDataTable(execSql, par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    var model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        #endregion

        #region private
        public tb_log CreateModel(DataRow dr)
        {
            var model = new tb_log();
            if (dr.Table.Columns.Contains("id") && dr["id"].ToString() != "")
            {
                model.id = int.Parse(dr["id"].ToString());
            }
            if (dr.Table.Columns.Contains("serviceid") && dr["serviceid"].ToString() != "")
            {
                model.serviceid = int.Parse(dr["serviceid"].ToString());
            }
            if (dr.Table.Columns.Contains("logtype") && dr["logtype"].ToString() != "")
            {
                model.logtype = int.Parse(dr["logtype"].ToString());
            }
            if (dr.Table.Columns.Contains("msg") && dr["msg"].ToString() != "")
            {
                model.msg = dr["msg"].ToString();
            }
            if (dr.Table.Columns.Contains("createtime") && dr["createtime"].ToString() != "")
            {
                model.createtime = DateTime.Parse(dr["createtime"].ToString());
            }
            if (dr.Table.Columns.Contains("servicename") && dr["servicename"].ToString() != "")
            {
                model.servicename = Convert.ToString(dr["servicename"]);
            }

            return model;
        }
        #endregion
    }
}