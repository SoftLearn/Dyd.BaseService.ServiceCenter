﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Service.Provider;
using XXF.BaseService.ServiceCenter.SystemRuntime;

namespace XXF.BaseService.ServiceCenter.Redis
{
    public class RedisNetCommand
    {
        private string redisServerIp;
        public RedisNetCommand(string rediserverip)
        {
            redisServerIp = rediserverip;
        }
        /// <summary>
        /// 消息发送
        /// </summary>
        /// <param name="mqpath"></param>
        public void SendMessage(ServiceNetCommand cmd)
        {
            try
            {
                var manager = new XXF.Redis.RedisManager();
                using (var db = manager.CreateClient(redisServerIp.Split(':')[0], Convert.ToInt32(redisServerIp.Split(':')[1]), ""))
                {
                    var i = db.GetClient().PublishMessage(SystemParamConfig.Redis_Channel + ".ServiceID_" + cmd.ServiceID, new XXF.Serialization.JsonHelper().Serializer(cmd));
                    db.GetClient().Quit();
                }

                //using (var c = manager.GetPoolClient(redisServerIp))
                //{
                //    var i = c.GetClient().PublishMessage(SystemParamConfig.Redis_Channel, new XXF.Serialization.JsonHelper().Serializer(cmd));
                //}
            }
            catch (Exception exp)
            {

            }
        }
    }
}
