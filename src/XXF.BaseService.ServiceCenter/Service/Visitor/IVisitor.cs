﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Service.Visitor
{
    /// <summary>
    /// 访问器接口
    /// </summary>
    public interface IVisitor
    {
        ServerVisitProvider ServerVisitor { get; }
    }
}
