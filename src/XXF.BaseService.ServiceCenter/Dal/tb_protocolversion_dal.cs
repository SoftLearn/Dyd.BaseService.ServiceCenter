using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ServiceCenter.Model;

namespace XXF.BaseService.ServiceCenter.Dal
{
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
	public partial class tb_protocolversion_dal
    {
        public virtual bool Add(DbConn PubConn, tb_protocolversion_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>()
                {
					
					//服务id
					new ProcedureParameter("@serviceid",    model.serviceid),
					//协议版本号
					new ProcedureParameter("@version",    model.version),
					//接口版本号
					new ProcedureParameter("@interfaceversion",    model.interfaceversion),
					//创建时间
					new ProcedureParameter("@createtime",    model.createtime),
					//协议json
					new ProcedureParameter("@protocoljson",    model.protocoljson)   
                };
            int rev = PubConn.ExecuteSql(@"insert into tb_protocolversion(serviceid,version,interfaceversion,createtime,protocoljson)
										   values(@serviceid,@version,@interfaceversion,@createtime,@protocoljson)", Par);
            return rev == 1;

        }

        public virtual bool Edit(DbConn PubConn, tb_protocolversion_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
                    
					//服务id
					new ProcedureParameter("@serviceid",    model.serviceid),
					//协议版本号
					new ProcedureParameter("@version",    model.version),
					//接口版本号
					new ProcedureParameter("@interfaceversion",    model.interfaceversion),
					//创建时间
					new ProcedureParameter("@createtime",    model.createtime),
					//协议json
					new ProcedureParameter("@protocoljson",    model.protocoljson)
            };
			Par.Add(new ProcedureParameter("@id",  model.id));

            int rev = PubConn.ExecuteSql("update tb_protocolversion set serviceid=@serviceid,version=@version,interfaceversion=@interfaceversion,createtime=@createtime,protocoljson=@protocoljson where id=@id", Par);
            return rev == 1;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id",  id));

            string Sql = "delete from tb_protocolversion where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual tb_protocolversion_model Get(DbConn PubConn, int serviceid, double version)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@serviceid", serviceid));
            Par.Add(new ProcedureParameter("@version", version));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_protocolversion s where s.version=@version and s.serviceid=@serviceid");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual tb_protocolversion_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_protocolversion s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
				return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

		public virtual tb_protocolversion_model CreateModel(DataRow dr)
        {
            var o = new tb_protocolversion_model();
			
			//
			if(dr.Table.Columns.Contains("id"))
			{
				o.id = dr["id"].Toint();
			}
			//服务id
			if(dr.Table.Columns.Contains("serviceid"))
			{
				o.serviceid = dr["serviceid"].Toint();
			}
			//协议版本号
			if(dr.Table.Columns.Contains("version"))
			{
				o.version = dr["version"].Toint();
			}
			//接口版本号
			if(dr.Table.Columns.Contains("interfaceversion"))
			{
				o.interfaceversion = dr["interfaceversion"].Todouble();
			}
			//创建时间
			if(dr.Table.Columns.Contains("createtime"))
			{
				o.createtime = dr["createtime"].ToDateTime();
			}
			//协议json
			if(dr.Table.Columns.Contains("protocoljson"))
			{
				o.protocoljson = dr["protocoljson"].ToString();
			}
			return o;
        }
    }
}