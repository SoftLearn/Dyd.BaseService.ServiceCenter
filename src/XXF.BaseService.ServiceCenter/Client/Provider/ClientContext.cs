﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Client.Provider
{
    /// <summary>
    /// 客户端上下文
    /// </summary>
    public class ClientContext
    {
        public Model.tb_service_model ServiceModel { get; set; }
        public List<Model.tb_node_model> NodeModels { get; set; }
        public Model.tb_client_model ClientModel { get; set; }
        public Model.tb_protocolversion_model ProtocolVersionModel { get; set; }
        public DateTime ServiceLastUpdateTime { get; set; }
        public ClientHeatBeatProtect HeatBeatProtect { get; set; }
    }
}
